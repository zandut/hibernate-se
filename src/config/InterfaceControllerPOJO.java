/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author PRAKTIKUM
 */
public interface InterfaceControllerPOJO<T> {
    public Session session = HibernateUtilSample.getSessionFactory().openSession();
    
    public boolean insert(T obj);
    public boolean edit(T obj);
    public boolean delete(T obj);
    
    public List<T> getALL();
    
    public T get(T obj);
    
    
}
