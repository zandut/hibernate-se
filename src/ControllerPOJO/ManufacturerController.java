/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerPOJO;

import Models.Manufacturer;
import config.InterfaceControllerPOJO;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author PRAKTIKUM
 */
public class ManufacturerController implements InterfaceControllerPOJO<Manufacturer>{

    @Override
    public boolean insert(Manufacturer obj) {
        try
        {
            session.getTransaction().begin();
            session.save(obj);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception ex)
        {
            
            ex.printStackTrace();
            return false;
        }
                
    }

    @Override
    public boolean edit(Manufacturer obj) {
        try
        {
            session.getTransaction().begin();
            session.update(obj);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception ex)
        {
            
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Manufacturer obj) {
        try
        {
            session.getTransaction().begin();
            session.delete(obj);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception ex)
        {
            
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Manufacturer> getALL() {
        Query query = session.createQuery("FROM Manufacturer");
        
        return query.list();
    }

    @Override
    public Manufacturer get(Manufacturer obj) {
        return (Manufacturer) session.get(Manufacturer.class, obj.getManufacturerId());
    }
    
}
