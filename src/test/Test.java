/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import ControllerPOJO.ManufacturerController;
import Models.Manufacturer;
import java.util.List;

/**
 *
 * @author PRAKTIKUM
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        // TODO code application logic here
        Manufacturer m = new Manufacturer(1400);
        m.setName("Fauzan Manufacturer123123123");
        m.setEmail("mfauzan613110035@gmail.com");
        m.setCity("Klaten");
        m.setPhone("087821334950");
        
        ManufacturerController mc = new ManufacturerController();
//        if (mc.insert(m))
//        {
//            System.out.println("Insert Berhasil !!!!");
//        }
//        else
//        {
//            System.out.println("Insert Gagal !!!!! ");
//        }
        
        System.out.println("======LIST MANUFACTURER=====");
        List<Manufacturer> list = mc.getALL();
        for (Manufacturer m1 : list)
        {
            System.out.println(m1.toString());
            System.out.println("-----------------------------------------------------------------------");
        }
        
        System.out.println("=======MANUFACTURER BY ID======");
        m.setManufacturerId(19984682);
        Manufacturer m2 = mc.get(m);
        System.out.println(m2.toString());
        
        
    }
    
}
